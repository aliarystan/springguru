package com.eureka.zuul.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * date on 2016-10-15.
 */

@RestController
@RequestMapping( value = "/test", produces = MediaType.APPLICATION_JSON_VALUE )
public class UserController {


    /*
     *  We are not using userService.findByUsername here(we could),
     *  so it is good that we are making sure that the user has role "ROLE_USER"
     *  to access this endpoint.
     */
    @RequestMapping("/whoami")
    @PreAuthorize("isAuthenticated()")
    public String user(Principal user) {
        return "test";
    }

}
