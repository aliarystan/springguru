package com.eureka.flux.service;


import com.eureka.flux.pojo.Student;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Repository
public interface StudentRepository extends ReactiveMongoRepository<Student, Long> {
}