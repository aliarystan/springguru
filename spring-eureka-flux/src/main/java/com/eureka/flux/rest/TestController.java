package com.eureka.flux.rest;


import com.eureka.flux.pojo.Student;
import com.eureka.flux.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@RequestMapping("adminDept")
@AllArgsConstructor
@RestController
public class TestController {

    @Autowired
    private StudentService studentService;

    @GetMapping
    public Flux<Student> getAll() {
        System.out.println("::will returns ALL Students records::");
        return studentService.getAll();
    }
    @GetMapping("/test")
    public Flux<String> testing() {
        return Flux.range(1,10).map(i->{ return
                Integer.toString(i)+"<br>";})
                .delayElements(Duration.ofMillis(100));
    }


    @GetMapping("/flux/one")
    public Mono<String> one() {
        return Mono.just("one");
    }

    @GetMapping("/flux/ten")
    public Flux<Integer> list() {
        return Flux.range(1, 10);
    }


    @GetMapping("{id}")
    public Mono<Student> getById(@PathVariable("id") final Long id) {
        System.out.println("::will return a Student record::");
        return studentService.getById(id);
    }


    @PutMapping("{id}")
    public Mono updateById(@PathVariable("id") final String id, @RequestBody final Student student) {
        System.out.println("::update the Student record::");
        return studentService.update(id, student);
    }

    @PostMapping
    public Mono save(@RequestBody final Student student) {
        System.out.println("will insert the student's record :: "+ student.getId() + " :: " + student.getFirstName());
        return studentService.save(student);
    }


    @GetMapping("/qwe")
    public List<Student> test() {
        List<Student> students = new ArrayList<Student>();
        System.out.println("::will return a Student record::");
        for (int x = 11; x <= 1000; x++) {
            Student st = new Student(new Long(x), "Serik", "Aliarystan");
            students.add(st);
        }
        return students;
    }

    @DeleteMapping("{id}")
    public Mono delete(@PathVariable final Long id) {
        System.out.println("::will delete a Student record::");
        return studentService.delete(id);
    }
}
