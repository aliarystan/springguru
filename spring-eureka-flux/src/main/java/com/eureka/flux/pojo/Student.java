package com.eureka.flux.pojo;


import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@Builder
@AllArgsConstructor
@Document
@ToString
@Getter
@Setter
public class Student {
    @Id
    private Long id;
    private String firstName;
    private String lastName;
}
