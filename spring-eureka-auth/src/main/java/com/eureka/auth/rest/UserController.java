package com.eureka.auth.rest;


import com.eureka.auth.model.User;
import com.eureka.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class UserController {

    @Autowired
    private UserRepository userService;


    /*
     *  We are not using userService.findByUsername here(we could),
     *  so it is good that we are making sure that the user has role "ROLE_USER"
     *  to access this endpoint.
     */
    @RequestMapping("/who")
    public User user(Principal user) {
        System.out.println(user);
        return this.userService.findByUsername(user.getName());
    }
    @GetMapping("/test")
    public String test() {
        return "test correct";
    }

}