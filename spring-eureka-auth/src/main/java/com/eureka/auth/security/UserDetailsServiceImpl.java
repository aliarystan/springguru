package com.eureka.auth.security;

import java.util.Arrays;
import java.util.List;

import com.eureka.auth.model.User;
import com.eureka.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service   // It has to be annotated with @Service.
public class UserDetailsServiceImpl implements UserDetailsService  {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		// hard coding the users. All passwords must be encoded.
		User user = userRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException
					("Username: " + username + " not found");
		} else {
			return user;
		}
	}
	
	// A (temporary) class represent the user saved in the database.
}