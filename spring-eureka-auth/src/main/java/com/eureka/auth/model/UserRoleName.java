package com.eureka.auth.model;

public enum UserRoleName {
    ROLE_USER,
    ROLE_CASHIER,
    ROLE_DISPATCHER,
    ROLE_TEAM,
    ROLE_ADMIN
}
