package com.eureka.flux.resolver;



import com.eureka.flux.model.Status;
import com.eureka.flux.model.StatusRepository;
import graphql.kickstart.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StatusResolver implements GraphQLQueryResolver {
    @Autowired
    private StatusRepository statusRepository;

    public List<Status> getStatus(Status status) {
        return statusRepository.findAll(Example.of(status));
    }

    public Status setStatus(Status status) {
        return statusRepository.save(status);
    }

}