package com.eureka.flux.controllers;

import com.eureka.flux.annotaion.WithUser;
import com.eureka.flux.util.UserInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {



    /*
     *  We are not using userService.findByUsername here(we could),
     *  so it is good that we are making sure that the user has role "ROLE_USER"
     *  to access this endpoint.
     */
    @GetMapping("/test")
    public UserInfo test(@WithUser UserInfo userInfo) {
        return userInfo;
    }

}